<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('halaman.form');
    }

    public function kirim(Request $request)
    {

        // dd($request->all());
            $firstname = $request['fname'];
            $lastname = $request['lname'];
            $gender = $request['gender'];
            $negara = $request['Country'];
            $bahasa = $request['Language'];
            $bio = $request['Bio'];

            return view('halaman.home', compact('firstname', 'lastname', 'gender', 'negara', 'bahasa', 'bio'));

    }
}
